import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static final String IN_FILE_TXT = "src\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\music";

    public static void main(String[] args) {
        extractMusicLinks();
        downloadMusicFiles();
    }

    /**
     * Скачивает найденные музыкальные файлы
     */
    private static void downloadMusicFiles() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String musicLink;
            int downloadedFilesCount = 0;
            try {
                while ((musicLink = musicFile.readLine()) != null) {
                    downloadUsingNIO(musicLink, PATH_TO_MUSIC + String.valueOf(downloadedFilesCount) + ".mp3");
                    downloadedFilesCount++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Извлекает из вебстраниц ссылки на музыку и сохраняет их в файл OUT_FILE_TXT
     */
    private static void extractMusicLinks() {
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {

            String webPageLink;
            while ((webPageLink = inFile.readLine()) != null) {

                URL webPageURL = new URL(webPageLink);

                String webPageHTML;
                /*
                 * openStream открывает поток ввода, связанный с веб-страницей
                 * InputStreamReader позволяет читать из InputStream символы, а не просто байты
                 * BufferedReader читает текст построчно
                 */
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(webPageURL.openStream()))) {

                    Stream<String> lines = bufferedReader.lines();
                    /*
                     * метод collect принимает на вход объект Collector, который
                     * определяет как нужно склеивать строки. Collectors.joining("\n")
                     * склеивает строки с помощью символа \n 
                     */
                    webPageHTML = lines.collect(Collectors.joining("\n"));
                }

                /*
                 * \\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")
                 * \\s - пробельный символ - табуляция, пробел и \n
                 * [^>] - все, кроме >, т.е. все кроме конца тега
                 */
                Pattern musicLinkPattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
                Matcher matcher = musicLinkPattern.matcher(webPageHTML);
                int i = 0;
                while (matcher.find() && i < 2) {
                    outFile.write(matcher.group() + "\r\n");
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Скачивает файл из Интернета по ссылке musicLink в файл на диске компьютера outputMusicFile
     *
     * @param musicLink ссылка на файл в Интернете
     * @param outputMusicFile путь к файлу, в который будет скачано содержимое из Интернета
     * @throws IOException если произошла ошибка ввода-вывода
     */
    private static void downloadUsingNIO(String musicLink, String outputMusicFile) throws IOException {
        URL musicURL = new URL(musicLink);
        /*
         * ReadableByteChannel - канал, способный только читать данные из файла. Каналы отличаются тем,
         * что они изначально написаны создателями Java для работы в многопоточных системах
         * Если к одному источнику подключены два ReadableByteChannel, они не могут одновременно читать данные
         * Сначала прочитает один канал, а второй будет ждать пока источник освободится
         */
        ReadableByteChannel byteChannel = Channels.newChannel(musicURL.openStream());
        FileOutputStream fileStream = new FileOutputStream(outputMusicFile);

        /*
         * Метод getChannel позволяет преобразовать поток вывода в файловый канал
         */
        FileChannel fileChannel = fileStream.getChannel();

        /*
         * Метод transferFrom позволяет прочитать данные из byteChannel в файл,
         * к которому привязан fileChannel. 0 - стартовая позиция для чтения 
         * Long.MAX_VALUE - количество прочитанных байт данных. MAX_VALUE - максимальное значение, которое
         * можно хранить в переменной типа long, означает что из источника мы прочитаем все байты
         */
        fileChannel.transferFrom(byteChannel, 0, Long.MAX_VALUE);
        fileStream.close();
        byteChannel.close();
    }
}
